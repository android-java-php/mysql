package org.mysql.formulario;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.mysql.conexion.Conexion;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */
public class Formulario extends javax.swing.JFrame  {

    /**
     * Creates new form Mysql
     */
public Formulario() {
initComponents();

}
public void es() throws SQLException, ClassNotFoundException{

}
private void crearUsuarioMysql() throws ClassNotFoundException{
Conexion mysql=new Conexion();
mysql.setServ(this.txtip.getText());
mysql.setUsr(this.txtusuario.getText());
mysql.setPass(this.txtpass.getText());
Connection cn=mysql.Conectar();
String usuario=txtusr2.getText();
String pss=txtpass2.getText();
String sql="CREATE USER '"+usuario+"'@'"+txtip.getText()+"' IDENTIFIED BY '"+pss+"';";
try{
Statement st=cn.createStatement();
st.executeUpdate(sql);
st.close();


JOptionPane.showMessageDialog(null,"usuario creado exitosamente");
crearPrivilegiosUsuarioMysql();

}catch(Exception e){
System.out.println("ErrorCrearUsuarios!!!");  
}
}


private void eliminarUsuarioMysql() throws ClassNotFoundException{
Conexion mysql=new Conexion();
mysql.setServ(this.txtip.getText());
mysql.setUsr(this.txtusuario.getText());
mysql.setPass(this.txtpass.getText());
Connection cn=mysql.Conectar();
String usuario=txtusr2.getText();
String pss=txtpass2.getText();
String sql="DROP USER '"+usuario+"'@'"+txtip.getText()+"';";
try{
Statement st=cn.createStatement();
st.executeUpdate(sql);
st.close();
JOptionPane.showMessageDialog(null,"usuario eliminado exitosamente");

}catch(Exception e){
System.out.println("ErrorEliminarUsuario!!!");  
}
}


private void crearPrivilegiosUsuarioMysql() throws ClassNotFoundException{
Conexion mysql=new Conexion();
mysql.setServ(this.txtip.getText());
mysql.setUsr(this.txtusuario.getText());
mysql.setPass(this.txtpass.getText());
Connection cn=mysql.Conectar();
String usuario=txtusr2.getText();
String pss=txtpass2.getText();
String sql="GRANT ALL PRIVILEGES ON "+txtprivilegios.getText()+" TO '"+usuario+"'@'"+txtip.getText()+"';";
try{
Statement st=cn.createStatement();
st.executeUpdate(sql);
st.close();
JOptionPane.showMessageDialog(null,"Privilegios: ALL PRIVILEGES otorgados");
}catch(Exception e){
System.out.println("ErrorCrearPrivilegiosUsuario!!!");  
}
}

private void crearBaseDeDatos() throws ClassNotFoundException, SQLException{
Conexion mysql=new Conexion();
mysql.setServ(this.txtip.getText());
mysql.setUsr(this.txtusuario.getText());
mysql.setPass(this.txtpass.getText());
Connection cn=mysql.Conectar();
try{
String sql="CREATE DATABASE "+txtbasededatos.getText();
Statement st=cn.createStatement();
st.executeUpdate(sql);
st.close();
JOptionPane.showMessageDialog(null,"base de datos creada exitosamente");
//System.out.println("base de datos creada exitosamente");
}catch(SQLException ex){
System.out.println("ErrorCrearBaseDeDatos!!!");
}
}

private void eliminarBaseDeDatos() throws ClassNotFoundException{
Conexion mysql=new Conexion();
mysql.setServ(this.txtip.getText());
mysql.setUsr(this.txtusuario.getText());
mysql.setPass(this.txtpass.getText());
Connection cn=mysql.Conectar();
String db=txtbasededatos.getText();
String sql="DROP DATABASE "+db;
try{
Statement st= cn.createStatement();
st.executeUpdate(sql);
st.close();
JOptionPane.showMessageDialog(null,"base de datos eliminada exitosamente");
}catch(Exception e){
System.out.println("ErrorEliminarBaseDeDatos!!!");
}
}


private void crearTabla() throws ClassNotFoundException{
Conexion mysql=new Conexion();
mysql.setServ(this.txtip.getText());
mysql.setUsr(this.txtusuario.getText());
mysql.setPass(this.txtpass.getText());
mysql.setDb(this.txtbasededatos.getText());
Connection cn=mysql.Conectar();
String sql="CREATE TABLE "+txttabla.getText()+"("+txtdatos.getText()+")";
try{
Statement st= cn.createStatement();
st.executeUpdate(sql);
st.close();
JOptionPane.showMessageDialog(null,"tabla creada exitosamente");
}catch(Exception e){
System.out.println("ErrorCrearTabla!!!");
}
}

private String verSiExisteBaseDeDatos(String database) throws ClassNotFoundException, SQLException{
String pregunta=database;
String respuesta="no";
Conexion mysql=new Conexion();
mysql.setServ(this.txtip.getText());
mysql.setUsr(this.txtusuario.getText());
mysql.setPass(this.txtpass.getText());
Connection cn=mysql.Conectar();
DatabaseMetaData meta = cn.getMetaData();
ResultSet rs = meta.getCatalogs();
while (rs.next()) {
String db=rs.getString("TABLE_CAT");
//System.out.println("TABLE_CAT = " + rs.getString("TABLE_CAT") );
if(pregunta.equals(db)){
respuesta="si";
}
}
rs.close();
return respuesta;
}

private String verSiExisteTabla(String database) throws ClassNotFoundException, SQLException{
String pregunta=database;
String respuesta="no";
DatabaseMetaData metadatos = null;
Conexion mysql=new Conexion();
mysql.setServ(this.txtip.getText());
mysql.setUsr(this.txtusuario.getText());
mysql.setPass(this.txtpass.getText());
mysql.setDb(txtbasededatos.getText());
Connection cn=mysql.Conectar();
metadatos = cn.getMetaData();
ResultSet rst;
rst = metadatos.getTables(null, null, null, null);
String tabla="";
while(rst.next()){
tabla = rst.getObject(3).toString();
if(pregunta.equals(tabla)){
respuesta="si";
}
}
rst.close();
return respuesta;
}

private void traerBasesDeDatos(String srv,String usr,String pass) throws ClassNotFoundException, SQLException{
Conexion mysql=new Conexion();
mysql.setServ(srv);
mysql.setUsr(usr);
mysql.setPass(pass);
Connection cn=mysql.Conectar();
DatabaseMetaData meta = cn.getMetaData();
ResultSet rs = meta.getCatalogs();
txtarea.setText(txtarea.getText()+"(CONEXION MYSQL:)"+"\n");
txtarea.setText(txtarea.getText()+"(IP:"+txtip.getText()+")"+"\n");
txtarea.setText(txtarea.getText()+"(USUARIO:"+txtusuario.getText()+")"+"\n");
txtarea.setText(txtarea.getText()+"(CONTRASEÑA:"+"xxxxxxxxxxxx"+")"+"\n");
txtarea.setText(txtarea.getText()+"(BASES DE DATOS:)"+"\n");
int cont =1;

while (rs.next()) {
String db=rs.getString("TABLE_CAT");
//System.out.println("TABLE_CAT = " + rs.getString("TABLE_CAT") );
//model.addRow(new Object[] {db});

txtarea.setText(txtarea.getText()+cont+")"+db+"\n");
cont++;
}
cont=cont-1;
txtarea.setText(txtarea.getText()+"(TOTAL:"+cont+")"+"\n");
txtarea.setText(txtarea.getText()+"(FINAL CONEXION)"+"\n");
rs.close();
}


private void traerTabla() throws ClassNotFoundException, SQLException{

DatabaseMetaData metadatos = null;
Conexion mysql=new Conexion();

mysql.setServ(txtip.getText());
mysql.setUsr(txtusuario.getText());
mysql.setPass(txtpass.getText());
mysql.setDb(txtbasededatos.getText());
Connection cn=mysql.Conectar();
metadatos = cn.getMetaData();
ResultSet rst;
rst = metadatos.getTables(null, null, null, null);
String tabla="";
int cont =1;
txtarea.setText(txtarea.getText()+"(CONEXION MYSQL:)"+"\n");
txtarea.setText(txtarea.getText()+"(IP:"+txtip.getText()+")"+"\n");
txtarea.setText(txtarea.getText()+"(USUARIO:"+txtusuario.getText()+")"+"\n");
txtarea.setText(txtarea.getText()+"(CONTRASEÑA:"+"xxxxxxxxxxxx"+")"+"\n");
txtarea.setText(txtarea.getText()+"(BASE DE DATOS:"+txtbasededatos.getText()+")"+"\n");
txtarea.setText(txtarea.getText()+"(TABLAS:)"+"\n");
while(rst.next()){
tabla = rst.getObject(3).toString();
//System.out.println("Nombre de Tabla: "+tabla);

txtarea.setText(txtarea.getText()+cont+")"+tabla+"\n");
cont++;
}
cont=cont-1;
txtarea.setText(txtarea.getText()+"(TOTAL:"+cont+")"+"\n");
txtarea.setText(txtarea.getText()+"(FINAL CONEXION)"+"\n");
cn.close();
}

private void traerUsuario() throws ClassNotFoundException, SQLException{

DatabaseMetaData metadatos = null;
Conexion mysql=new Conexion();

mysql.setServ(txtip.getText());
mysql.setUsr(txtusuario.getText());
mysql.setPass(txtpass.getText());

Connection cn=mysql.Conectar();
metadatos = cn.getMetaData();
Statement st=cn.createStatement();
ResultSet rst=st.executeQuery("select User from mysql.user;");
//ResultSet rst=st.executeQuery("select User from mysql.user where User='"+"root"+"';");
String tabla="";
int cont =1;
txtarea.setText(txtarea.getText()+"(CONEXION MYSQL:)"+"\n");
txtarea.setText(txtarea.getText()+"(IP:"+txtip.getText()+")"+"\n");
txtarea.setText(txtarea.getText()+"(USUARIO:"+txtusuario.getText()+")"+"\n");
txtarea.setText(txtarea.getText()+"(CONTRASEÑA:"+"xxxxxxxxxxxx"+")"+"\n");
txtarea.setText(txtarea.getText()+"(Usuarios:)"+"\n");
while(rst.next()){
tabla = rst.getString(1);
//System.out.println("Nombre de Tabla: "+tabla);

txtarea.setText(txtarea.getText()+cont+")"+tabla+"\n");
cont++;
}
cont=cont-1;
txtarea.setText(txtarea.getText()+"(TOTAL:"+cont+")"+"\n");
txtarea.setText(txtarea.getText()+"(FINAL CONEXION)"+"\n");
cn.close();
}





private void existeUsuario() throws ClassNotFoundException, SQLException{

DatabaseMetaData metadatos = null;
Conexion mysql=new Conexion();

mysql.setServ(txtip.getText());
mysql.setUsr(txtusuario.getText());
mysql.setPass(txtpass.getText());

Connection cn=mysql.Conectar();
metadatos = cn.getMetaData();
Statement st=cn.createStatement();
//ResultSet rst=st.executeQuery("select User from mysql.user;");
ResultSet rst=st.executeQuery("select User from mysql.user where User='"+txtusr2.getText()+"';");
String tabla="";
int cont =1;
txtarea.setText(txtarea.getText()+"(CONEXION MYSQL:)"+"\n");
txtarea.setText(txtarea.getText()+"(IP:"+txtip.getText()+")"+"\n");
txtarea.setText(txtarea.getText()+"(USUARIO:"+txtusuario.getText()+")"+"\n");
txtarea.setText(txtarea.getText()+"(CONTRASEÑA:"+"xxxxxxxxxxxx"+")"+"\n");
txtarea.setText(txtarea.getText()+"(Usuarios:)"+"\n");
while(rst.next()){
tabla = rst.getString(1);
//System.out.println("Nombre de Tabla: "+tabla);

txtarea.setText(txtarea.getText()+cont+")"+tabla+"\n");
cont++;
}
cont=cont-1;
txtarea.setText(txtarea.getText()+"(TOTAL:"+cont+")"+"\n");
txtarea.setText(txtarea.getText()+"(FINAL CONEXION)"+"\n");
cn.close();
}





    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtbasededatos = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton9 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        txtusr2 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtprivilegios = new javax.swing.JTextField();
        txtpass2 = new javax.swing.JPasswordField();
        jButton7 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txttabla = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtdatos = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtusuario = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtip = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtpass = new javax.swing.JPasswordField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtarea = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        jMenuItem1.setText("Borrar");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Base de Datos:"));

        jLabel1.setText("Nombre de la Base de Datos:");

        jButton1.setText("Crear");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Eliminar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Consultar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtbasededatos)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtbasededatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel3.setText("Usuario:");

        jLabel4.setText("Contraseña:");

        jButton9.setText("Eliminar");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton8.setText("Crear");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        txtusr2.setToolTipText("");

        jLabel10.setText("Privilegios:");

        txtprivilegios.setToolTipText("");

        jButton7.setText("Consultar");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtusr2)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtprivilegios, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton7)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txtpass2))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtusr2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtpass2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtprivilegios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton8)
                    .addComponent(jButton9)
                    .addComponent(jButton7))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel5.setText("Nombre Tabla:");

        jLabel6.setText("Listado de campos y sus tipos de datos:");

        jButton4.setText("Crear");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Eliminar");

        jButton6.setText("Consultar");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtdatos)
                    .addComponent(txttabla, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton6)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttabla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtdatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton4)
                    .addComponent(jButton5)
                    .addComponent(jButton6))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel7.setText("Usuario:");

        jLabel8.setText("IP:");

        jLabel9.setText("Contraseña:");

        txtarea.setColumns(20);
        txtarea.setRows(5);
        txtarea.setComponentPopupMenu(jPopupMenu1);
        jScrollPane1.setViewportView(txtarea);

        jMenu3.setText("Ver Usuarios");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu3MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        jMenu1.setText("Ver Bases de Datos");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu1MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Ver Tablas");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu2MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtpass, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtip)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(txtip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)
                            .addComponent(txtpass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(55, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
if(txtip.getText().length()!=0 && txtusuario.getText().length()!=0){
if(txtbasededatos.getText().toString().length()!=0){
try {   
String respuesta=verSiExisteBaseDeDatos(txtbasededatos.getText());
if(respuesta.equals("no")){
crearBaseDeDatos() ;
}else{
JOptionPane.showMessageDialog(null,"Existe Base de datos: "+txtbasededatos.getText());   
}
} catch (ClassNotFoundException ex) {
Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
} catch (SQLException ex) {
Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
}
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
if(txtip.getText().length()!=0 && txtusuario.getText().length()!=0){
if(txtbasededatos.getText().toString().length()!=0){
try {
String respuesta=verSiExisteBaseDeDatos(txtbasededatos.getText());
if(respuesta.equals("si")){
eliminarBaseDeDatos();
}else{
JOptionPane.showMessageDialog(null,"No existe Base de datos: "+txtbasededatos.getText());   
}
} catch (ClassNotFoundException ex) {
}   catch (SQLException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    }
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
if(txtip.getText().length()!=0 && txtusuario.getText().length()!=0){

if(txttabla.getText().toString().length()!=0 && txtdatos.getText().toString().length()!=0){

    

try { 


crearTabla();

} catch (ClassNotFoundException ex) {
Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
}



}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
if(txtip.getText().length()!=0 && txtusuario.getText().length()!=0){
if(txtbasededatos.getText().toString().length()!=0){
    try {
  
String respuesta=verSiExisteBaseDeDatos(txtbasededatos.getText());
   JOptionPane.showMessageDialog(null,"Existe BaseDeDatos: "+respuesta);   
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    }
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
if(txtip.getText().length()!=0 && txtusuario.getText().length()!=0){
if(txtusr2.getText().length()!=0){
try {
        crearUsuarioMysql() ;        // TODO add your handling code here:
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    }
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jMenu2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MousePressed
if(txtip.getText().length()!=0 && txtusuario.getText().length()!=0){
if(txtbasededatos.getText().toString().length()!=0){
    try {
        traerTabla();
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    }
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
    }//GEN-LAST:event_jMenu2MousePressed

    private void jMenu1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MousePressed
if(txtip.getText().length()!=0 && txtusuario.getText().length()!=0){
    try {
        traerBasesDeDatos(txtip.getText(),txtusuario.getText(),txtpass.getText());
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    }
}
    }//GEN-LAST:event_jMenu1MousePressed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
if(txtip.getText().length()!=0 && txtusuario.getText().length()!=0){
if(txtusr2.getText().length()!=0){
try {
        eliminarUsuarioMysql();        // TODO add your handling code here:
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    }
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
}else{
JOptionPane.showMessageDialog(null,"Introduzca datos!!!");
}
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
 txtarea.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
    try {
        String respuesta=verSiExisteTabla(txttabla.getText());
        if(respuesta.equals("si")){
        JOptionPane.showMessageDialog(null,"Existe Tabla: "+respuesta);
        }
        else{
         JOptionPane.showMessageDialog(null,"Existe Tabla: "+respuesta);
        }
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jMenu3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MousePressed
    try {
        traerUsuario();        // TODO add your handling code here:
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    }
    }//GEN-LAST:event_jMenu3MousePressed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
    try {
        existeUsuario();        // TODO add your handling code here:
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex) {
        Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
    }
    }//GEN-LAST:event_jButton7ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Formulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Formulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Formulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Formulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Formulario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtarea;
    private javax.swing.JTextField txtbasededatos;
    private javax.swing.JTextField txtdatos;
    private javax.swing.JTextField txtip;
    private javax.swing.JPasswordField txtpass;
    private javax.swing.JPasswordField txtpass2;
    private javax.swing.JTextField txtprivilegios;
    private javax.swing.JTextField txttabla;
    private javax.swing.JTextField txtusr2;
    private javax.swing.JTextField txtusuario;
    // End of variables declaration//GEN-END:variables

}
