package org.mysql.formulario;


import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.mysql.conexion.Conexion;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */
public class VerNombresBaseDeDatos extends javax.swing.JFrame {
private DefaultTableModel model=new DefaultTableModel();
private String srv;
private String usr;
private String pass;
    /**
     * Creates new form VerNombresBaseDeDatos
     */
public VerNombresBaseDeDatos() {
initComponents();
table1.setModel(model);
model.addColumn("Bases de Datos:");
}

public void traerBasesDeDatos(String srv,String usr,String pass) throws ClassNotFoundException, SQLException{
model= (DefaultTableModel)table1.getModel();
while(model.getRowCount()>0)model.removeRow(0);
Conexion mysql=new Conexion();
this.srv=srv;
this.usr=usr;
this.pass=pass;
mysql.setServ(this.srv);
mysql.setUsr(this.usr);
mysql.setPass(this.pass);
Connection cn=mysql.Conectar();
DatabaseMetaData meta = cn.getMetaData();
ResultSet rs = meta.getCatalogs();
while (rs.next()) {
String db=rs.getString("TABLE_CAT");
//System.out.println("TABLE_CAT = " + rs.getString("TABLE_CAT") );
model.addRow(new Object[] {db});
}
rs.close();
}

public void eliminar() throws ClassNotFoundException{
int num=table1.getSelectedRow();
String db=(String) table1.getValueAt(num,0);
Conexion mysql=new Conexion();
mysql.setServ(this.srv);
mysql.setUsr(this.usr);
mysql.setPass(this.pass);
Connection cn=mysql.Conectar();
String sql="DROP DATABASE "+db;
try{
Statement st= cn.createStatement();
st.executeUpdate(sql);
st.close();
JOptionPane.showMessageDialog(null,"base de datos eliminada exitosamente");
}catch(Exception e){

}
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        bteliminar = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        table1 = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();

        bteliminar.setText("Eliminar");
        bteliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bteliminarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(bteliminar);

        jMenuItem1.setText("Ver");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        table1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        table1.setComponentPopupMenu(jPopupMenu1);
        jScrollPane1.setViewportView(table1);

        jMenu1.setText("Actualizar");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu1MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bteliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bteliminarActionPerformed
    try {
        eliminar();
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(VerNombresBaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
    }
    }//GEN-LAST:event_bteliminarActionPerformed

    private void jMenu1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MousePressed
    try {
        traerBasesDeDatos(this.srv,this.usr,this.pass);        // TODO add your handling code here:
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(VerNombresBaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex) {
        Logger.getLogger(VerNombresBaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
    }
    }//GEN-LAST:event_jMenu1MousePressed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
VerTablasBaseDeDatos vr=new VerTablasBaseDeDatos();
int tb=table1.getSelectedRow();
String db=(String) table1.getValueAt(tb,0);
vr.setVisible(true);
    try {
        vr.traerTabla(this.srv, this.usr,this.pass,db);
    } catch (ClassNotFoundException ex) {
        
    } catch (SQLException ex) {

    }  
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VerNombresBaseDeDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VerNombresBaseDeDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VerNombresBaseDeDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VerNombresBaseDeDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VerNombresBaseDeDatos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem bteliminar;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table1;
    // End of variables declaration//GEN-END:variables
}
