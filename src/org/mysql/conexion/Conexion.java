package org.mysql.conexion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */

public class Conexion {
private String db="";
private String serv="";
private String url="";
private String usr="";
private String pass="";

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getServ() {
        return serv;
    }

    public void setServ(String serv) {
        this.serv = serv;
    }

    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
public Conexion(){
}
public Connection Conectar() throws ClassNotFoundException{
url="jdbc:mysql://"+serv+"/"+db;
Connection link=null;
try{
Class.forName("com.mysql.jdbc.Driver");
link=DriverManager.getConnection(this.url,this.usr,this.pass);
System.out.println("Se ha iniciado la conexión con el servidor de forma exitosa");
        
}catch(Exception ex){
System.out.print("error en conexion");
}
return link;
}
}
